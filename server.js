const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({
  extended: true
}));

const MongoClient = require('mongodb').MongoClient;
var db;

MongoClient.connect('mongodb://nikita:123987@ds011725.mlab.com:11725/test-version-nikita', (err, database) => {

  if (err) return console.log(err);
  db = database
  app.listen(3000, () => {
    console.log('listening on 3000, db connected');
  });

});

//app.listen(3000, function () {
//  console.log('listening on 3000 YEEA!');
//});

app.get('/', (req, res) => { // function(req, res) {...}
  console.log("/index.html");
  //res.sendFile(__dirname + '/index.html'); 
  
  //var cursor = db.collection('quotes').find();
  //console.log(cursor);
  
  db.collection('quotes').find().toArray(function(err, results) {
    if (err) return console.log(err);
    //console.log(results)

    // render index.ejs
    res.render('index.ejs', {quotes: results});
    
    // send HTML file populated with quotes here
  })
  
  // Note: __dirname is the path to your current working directory. Try logging it and see what you get!
  // Mine was '/Users/zellwk/Projects/demo-repos/crud-express-mongo' for this app.
});


 


//app.post('/quotes', (req, res) => {
//  console.log('Hellooooooooooooooooo!')
//}) 
// send data to db
app.post('/quotes', (req, res) => {
  console.log('/quotes entered');
  db.collection('quotes').save(req.body, (err, result) => {
    if (err) return console.log(err)

    console.log('+ saved to database'); 
  }); 
  res.redirect('/'); // redirect user back to start page
});